package pages_Bankbazzar;

import java.util.List;

import org.openqa.selenium.WebElement;


import wdMethods.ProjectMethods;


public class InvestMutualFunds extends ProjectMethods {

 public InvestMutualFunds investmenttype() throws InterruptedException
 {
	 Thread.sleep(3000);
	List<WebElement> Listofinvestmentstype = driver.findElementsByClassName("js-offer-name");
	for (WebElement eachelement : Listofinvestmentstype) {
		System.out.println(eachelement.getText());
		
	}
	return this;
	
 }
 public InvestMutualFunds investmentamount()
 {
	 
	List<WebElement> Listofinvestmentsvalue = driver.findElementsByXPath("//div[@class='offer-section-column col-same-height col-middle investment-amount']/span[1]");
	for (WebElement eachelement : Listofinvestmentsvalue) {
		System.out.println(eachelement.getText());
		
	}
	return this;
	
 }

}
