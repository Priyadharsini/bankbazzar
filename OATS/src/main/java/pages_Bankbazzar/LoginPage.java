package pages_Bankbazzar;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;

import wdMethods.ProjectMethods;
import wdMethods.SeMethods;


public class LoginPage extends ProjectMethods {

 public LoginPage mouseoverinvestment()
 {
	WebElement mousehover = locateElement("Linktext", "INVESTMENTS"); 
	mouseOver(mousehover);
	return this;
	
 }
 public MutualFundsPage clickmutualfunds() throws InterruptedException
 {
	 Thread.sleep(3000);
WebElement elemutualfunds = locateElement("Linktext", "Mutual Funds");
click(elemutualfunds);
return new MutualFundsPage();

 }
 



}
