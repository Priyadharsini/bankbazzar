package pages_Bankbazzar;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;

import wdMethods.ProjectMethods;
import wdMethods.SeMethods;


public class MutualFundsPage extends ProjectMethods {

 public MutualFundsPage clickmutualfundsbutton() throws InterruptedException
 {
	 Thread.sleep(7000);
	WebElement mutualfundsbutton = locateElement("linktext", "Search for Mutual Funds"); 
	click(mutualfundsbutton);
	return this;
	
 }
 public MutualFundsPage chooseage() throws InterruptedException
 {
	 int Age=24;
	 Thread.sleep(3000);
	 WebElement Ageslider = locateElement("xpath", "//div[@class='rangeslider__handle-label']");
	act.dragAndDropBy(Ageslider,(Age-18)*8, 0).perform();
	return this;
	
 }
 public MutualFundsPage chooseYear()
 {
	 WebElement Year = locateElement("linktext", "Jun 1994");
	 click(Year);
	return this;
	
 }
 public MutualFundsPage chooseDate()
 {
	 WebElement Date = locateElement("xpath", "//div[text()='19']");
	 click(Date);
	return this;
	
 }
 
 public MutualFundsPage verifyDOB()
 {
	WebElement DOB = locateElement("xpath", "//span[text()='19 Jun 1994']");
	System.out.println("The Date of Birth is "+getText(DOB));
	return this;
 }

public MutualFundsPage clickcontinue()
{
	 WebElement continuebtn = locateElement("linktext", "Continue");
	 click(continuebtn);
	return this;
	
}
public MutualFundsPage choosesalary() throws InterruptedException
{
	 
	 Thread.sleep(3000);
	 WebElement Salarysilder = locateElement("xpath", "//div[@class='rangeslider__handle-label']");
	act.dragAndDropBy(Salarysilder,157, 0).perform();
	return this;
	
}
public MutualFundsPage choosebank() throws InterruptedException
{
	 
	 Thread.sleep(3000);
	 WebElement choosebank = locateElement("xpath", "//span[text()='HDFC']/following::input[1]");
	 click(choosebank);
	return this;
	
}
public MutualFundsPage typeFirstname() throws InterruptedException 
{ 
	Thread.sleep(3000);
	 WebElement Firstname = locateElement("xpath", "//input[@name='firstName']");
	 type(Firstname, "Priya");
	return this;
	
}
public InvestMutualFunds clickViewMutualFunds()
{
	 WebElement ViewMutualFunds = locateElement("linktext", "View Mutual Funds");
	 click(ViewMutualFunds);
	return new InvestMutualFunds();
	
}

}
