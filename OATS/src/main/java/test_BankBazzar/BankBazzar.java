package test_BankBazzar;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import pages_Bankbazzar.LoginPage;
import utils.HtmlReporter;
import wdMethods.ProjectMethods;


public class BankBazzar extends ProjectMethods  {
	
	@BeforeClass
	public void setData() {
		testCaseName = "TC_01_BankBazzar";
		testCaseDescription ="TC_01";
		category = "Smoke";
		author= "Priya";
		
	}
	
@Test
	public  void Testcase_01() throws InterruptedException   {
		new LoginPage()
		.mouseoverinvestment()
		.clickmutualfunds()
		.clickmutualfundsbutton()
		.chooseage()
		.chooseYear()
		.chooseDate()
		.verifyDOB()
		.clickcontinue()
		.choosesalary()
		.clickcontinue()
		.choosebank()
		.typeFirstname()
		.clickViewMutualFunds()
		.investmenttype()
		.investmentamount();
		
	     
		}


}